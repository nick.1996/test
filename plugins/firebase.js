import * as firebase from 'firebase/app';
import 'firebase/firestore';

let firebaseConfig = {
  apiKey: "AIzaSyBOlc6ut7eKLGfTMeppJzYUtKuHHiDQ_ko",
  authDomain: "train-ticket-558df.firebaseapp.com",
  databaseURL: "https://train-ticket-558df.firebaseio.com",
  projectId: "train-ticket-558df",
  storageBucket: "train-ticket-558df.appspot.com",
  messagingSenderId: "1020276974414",
  appId: "1:1020276974414:web:98dfae1ce2b705665bf41f"
}

// 確認firebase是否準備初始化
if(!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

export const db = firebase.firestore();