
export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
      ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { 
    color: 'orange',
    failedColor: 'red',
    height: '5px',
    duration: 1000
},
  /*
  ** Global CSS
  */
  css: [
    '~/assets/css/main.scss',
    'iview/dist/styles/iview.css',
    'assets/css/padding-margin.css',
    'quill/dist/quill.snow.css',
    'quill/dist/quill.bubble.css',
    'quill/dist/quill.core.css'

  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/iview',
    '~/plugins/moment',
    '~/plugins/axios',
    '~/plugins/firebase',
    {src: "~plugins/vue-quill-editor.js", ssr: false}
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/proxy',
    ['nuxt-i18n', { 
      locales: [ 
        { 
          name: '繁體', 
          code: 'zh-TW', 
          iso: 'zh-TW', 
          file: 'zh-TW.js' 
        }, 
        { 
          name: 'English', 
          code: 'en', 
          iso: 'en-US', 
          file: 'en.js' 
        } 
      ], 
      lazy:true, 
      langDir: 'lang/', 
      defaultLocale: 'en', 
      strategy: 'no_prefix', 
      detectBrowserLanguage: { 
        // 將目前指定語系寫入cookie
        useCookie: true, 
        cookieKey: 'lang', 
        alwaysRedirect: false 
      } 
    }] 
  ], 
  axios: {
    proxy: true,
    prefix: '/api',
    credentials: true,
  },
  router: {
    base: '/test/',
  },
  generate: {
    dir: 'public',
  },
  proxy: {
    '/api': {
      target: 'https://ptx.transportdata.tw/MOTC/v2/Rail/TRA/', 
      changeOrigin: true, 
      pathRewrite: {
        '^/api': '',
      }
    }
  },
  /*
  ** Build configuration
  */
  build: {
  },
  dir: {
    pages: 'test',
  },
  /*
  ** You can extend webpack config here
  */
  extend (config, ctx) {
  }
}